#include"command.h"

#include <QDataStream>

Command::Command(const CommandType& t, const QVariant& d):
    type{t}, data{d} {}

void Command::setType(CommandType typeSet)
{
    type = typeSet;
}

void Command::setData(QVariant dataSet)
{
    data = dataSet;
}

CommandType Command::getType() const
{
    return type;
}

QVariant Command::getData() const
{
    return data;
}

QDataStream& operator>>(QDataStream& str, Command& cmd)
{
    CommandType typeSet;
    QVariant dataSet;
    str >> typeSet >> dataSet;
    cmd.setType(typeSet);
    cmd.setData(dataSet);
    return str;
}

QDataStream& operator<<(QDataStream& str, const Command& cmd)
{
    str << cmd.getType() << cmd.getData();
    return str;
}
