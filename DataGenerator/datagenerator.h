#ifndef DATAGENERATOR_H
#define DATAGENERATOR_H

#include "command.h"
#include "plugin.h"

#include <QDebug>
#include <QTimer>
#include <QtMath>
//#include <QRandomGenerator>

class DataGenerator : public QObject, public Plugin
{
  Q_OBJECT
  Q_INTERFACES(Plugin)
  Q_PLUGIN_METADATA(IID "ru.PlatformPlugins.DataGenerator/1.0" FILE "DataGenerator.json")
public:
    //explicit DataGenerator(QObject *parent = nullptr);
    void setParams(const QVariant& params) override;
    QVector<CommandHandler> init() override;
    void start() override;
    void EXPfunc(Command& cmd);
private:
    double m_sinMagn;
    double m_sinVal = 0;
    qreal m_sinAngle = 0;
    qreal m_sinAngleStep;
    static constexpr qreal m_k_maxAngle = 2 * M_PI;
    void incrementAngle();
    QTimer m_timer;
    //QRandomGenerator random;
    double m_noiseMagnitude;
    double m_noiseVal;
};

#endif // DATAGENERATOR_H
