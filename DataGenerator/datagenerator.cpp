#include "datagenerator.h"
#include "command.h"
//DataGenerator::DataGenerator(QObject *parent): Plugin(parent){}

void DataGenerator::setParams(const QVariant& params)
{
    QList<QVariant> paramsList = params.toList();
    int sampleRate = paramsList[0].toInt(); // In Hz
    m_noiseMagnitude = paramsList[1].toDouble();
    int sinFreq = paramsList[2].toInt(); // In Hz
    m_sinMagn = paramsList[3].toDouble();

    m_timer.setInterval(1000 / sinFreq / sampleRate);

    m_sinAngleStep = m_k_maxAngle / sampleRate;
    qDebug() << m_noiseMagnitude << m_sinMagn << m_timer.interval() << m_sinAngleStep;
}

void DataGenerator::start()
{
    connect(&m_timer, &QTimer::timeout, [&](){
        m_sinVal = m_sinMagn * qSin(m_sinAngle);
        incrementAngle();
        m_noiseVal = 0;//random.bounded(m_noiseMagnitude);
        qDebug() << m_sinVal << m_noiseVal;
        //        emit sendCommand({
        //                             {CommandType::UPDATE, "Tag"},
        //                             QVariant(QList<QVariant>({m_sinVal, m_noiseVal}))
        //                         });
    });
    m_timer.start();
}

void DataGenerator::incrementAngle()
{
    if (m_sinAngle >= m_k_maxAngle)
    {
        m_sinAngle = 0;
    } else {
        m_sinAngle += m_sinAngleStep;
    }
}

void DataGenerator::EXPfunc(Command& cmd)
{
  cmd.setData(m_sinVal);
}

QVector<CommandHandler> DataGenerator::init()
{
  QVector<CommandHandler> vch;
  CommandHandler ch;
  ch.h = [=](Command& a){this->EXPfunc(a);};
  ch.owner = this;
  //ch.type = CommandType(CommandType::Operator::EXECUTE, );
  vch.append(ch);
  return vch;
}

//Q_EXPORT_PLUGIN2(Plugin, DataGenerator)
