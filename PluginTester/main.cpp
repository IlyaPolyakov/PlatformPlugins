#include <QCoreApplication>
#include <QDebug>
#include <QPluginLoader>
#include "plugin.h"

#include <QDir>
int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);
  QDir plugdir("C:/work/PlatformPlugins/Debug/DataGenerator/debug");
  QVector<CommandHandler> cmds;
  foreach( const QString& pluginName, plugdir.entryList( QDir::Files ) ){
    QPluginLoader loader(plugdir.absoluteFilePath(pluginName));
    if(!loader.load())
      qDebug() << pluginName << " didn\'t download";
    else{
      qDebug() << "---------------------------";
      qDebug() << "Found: " << pluginName;
      if(Plugin *pl = qobject_cast<Plugin *> (loader.instance())){
        qDebug() << "This is plugin!";
        cmds += pl->init();
      }
      loader.unload();
      qDebug() << "---------------------------";
    }
  }
  cmds.clear();
  return a.exec();
}
