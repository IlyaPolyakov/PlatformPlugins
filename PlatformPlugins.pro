TEMPLATE = subdirs

SUBDIRS += \
    PluginTester \
    DataGenerator

OTHER_FILES += \
    hdr/command.h \
    src/command.cpp \
    hdr/commandtype.h \
    src/commandtype.cpp \
    hdr/platforminterface.h \
    hdr/plugin.h
