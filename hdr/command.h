#ifndef COMMAND_H
#define COMMAND_H
#include <QCoreApplication>
#include "commandtype.h"
#include <QVariant>

//представляет собой типизированное сообщение (или команду),
//используется для обмена между плагином и менеджером соединений.
//Содержит пару: тип команды и данные (объект действия команды).
//объявлены дружественные функции (де)сериализации (из)в поток(а).
class Command {
  public:
    Command() {}
    Command(const CommandType& t, const QVariant& d);
    void setType(CommandType typeSet);
    void setData(QVariant dataSet);
    CommandType getType() const;
    QVariant getData() const;
private:
    CommandType type;
    QVariant data;
    friend QDataStream& operator>>(QDataStream& str, Command& cmd);
    friend QDataStream& operator<<(QDataStream& str, const Command& cmd);
};
// регистрируем тип в QVariant
Q_DECLARE_METATYPE(Command)

//где-то в приложении должны быть вызваны до того, как будет установлено первое соединение:

// Чтобы использовать тип T в подключенных к очереди сигнальных и слот-соединениях
//int i = qRegisterMetaType<Command>();
// Регистрирует операторы потока для типа. Используются при потоковой передаче QVariant
//qRegisterMetaTypeStreamOperators<CommandType>("Command"); // пока ругается
#endif // COMMAND_H
