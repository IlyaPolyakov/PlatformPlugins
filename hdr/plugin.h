#ifndef PLUGIN_H
#define PLUGIN_H

#include "command.h"
#include "commandtype.h"
#include "platforminterface.h"

#include <QObject>
#include <QPointer>
#include <QtPlugin>
#include <QVariant>

class CommandHandler
{
  public:
    //~CommandHandler() {}
    CommandHandler() : type(){}
    CommandType type;
    QPointer<QObject> owner;
    std::function<void(Command&)> h;
};//+

class Plugin : public PlatformInterface
{
  public:
    ~Plugin() {}
    virtual void setParams(const QVariant& params) = 0;//-
    virtual QVector<CommandHandler> init() = 0;
    virtual void start() = 0;
};//+

Q_DECLARE_INTERFACE( Plugin, "ru.PlatformPlugins.DataGenerator/1.0" )

#endif // PLUGIN_H
